---
layout: post
title: Jekyll Trick 1
---

Wow. Jekyll is absurdly powerful. I never realized it until it came to mind that the main page of my blog was cluttered. Why? Because the entire content of all posts was being displayed on a single page.

So, what can you do about that? Well, one thing you can do is make a basic layout for a page. I called this `post.html` in the `_layouts` folder as it was for containing each and every post individually. To do this, I had to change my loop for the front page (adding every post and its content) to just a single post without a link for the title. This was incredibly easy, but then I thought, "Now I have the same post information in two places. Why would the user want to see an entire post on the front page? Ah, I should truncate it."

Truncation can be annoying in a lot of ways. My first idea was to take the post and to truncate the post two a word limit, strip the HTML, and add that to the front page. However, if you're limiting to a word limit and stripping HTML, you might end up mid sentence or you might end up adding code without syntax highlighting and that's just a mess.

<!-- more -->

The solution? To add a `<!-- concat -->` tag in each markdown post. I used the following code for each post:

{% highlight html %}
{% raw %}
<p class="post-content">
  {{ post.content | split:"<!-- concat -->" | first }}

  {% if post.content contains "<!-- concat -->" %}
    <a class="btn btn-primary" href="{{ post.url }}" role="button">Read More of Post</a>
  {% endif %}

</p>
{% endraw %}
{% endhighlight %}

Thanks Jekyll! Since Jekyll allows for full access of liquid tags, I was able to make an if statement checking for basically: `if x in y`. The entire logic is:

- Check the post has the `<!-- concat -->` tag
  - If it has it, truncate the post
  - If it does not have it, leave the post alone
- If the post does contain the `<!-- concat -->` tag, you also have to add a button. This button links to `{{ post.url }}`

Hope this helps someone! I made a bunch of small changes tonight on my blog. Looks good (at least to me `:)` )!
