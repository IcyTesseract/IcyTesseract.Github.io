---
layout: post
title: Hello World!
---

Merry Christmas everyone! Hopefully I'll get in the habit of posting again.

{% highlight javascript %}
if (youWereNice) {
  console.log("Ho, Ho, Ho, Merry Christmas!");
}
{% endhighlight %}
