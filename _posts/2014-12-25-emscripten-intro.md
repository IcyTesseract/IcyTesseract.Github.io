---
layout: post
title: Intro to Emscripten
---

I'm going to make a quick post about Emscripten. "So what is Emscripten?"

> Emscripten is a type of compiler termed a source-to-source compiler or transcompiler. As its input it takes LLVM bytecode, typically created by compiling from C or C++. As output it emits a file in the JavaScript programming language which can run in web browsers. -- [Source](en.wikipedia.org/wiki/Emscripten)

"What is LLVM?"

> LLVM is a library that is used to construct, optimize and produce intermediate and/or binary machine code. LLVM can be used as a compiler framework, where you provide the "front end" (parser and lexer) and the "back end" (code that converts LLVM's representation to actual machine code). -- [Source](stackoverflow.com/questions/2354725/what-exactly-is-llvm)

"So I heard that Emscripten is frequently faster than hand written Javascript code. Is that true?"

> Emscripten generates fast code — its default output format is asm.js , a highly optimizable subset of JavaScript that can execute at close to native speed in many cases.

"Ok, but now I have another question. What is asm.js?"

> Asm.js is a very strict subset of the Javascript language. Thus, it is just as portable as Javascript. What asm.js does is allow improvements in performance for web applications that are written in a statically typed language that uses manual memory management (such as C).

<!-- more -->

"What is so special about this?"

> Emscripten allows for anything from porting OpenGL ES to the web to porting massive code bases like Unreal Engine 3, Doom, Unity3D, and more.

Enough Q/A, I just wanted to make a post before I got started on another Emscripten application I was thinking up. Maybe I'll post some tutorials and tips on using Emscripten to transpile C to Javascript (C is my favorite language currently).

Also, Merry Christmas! Or, if you're not Christian, Happy Holidays!
