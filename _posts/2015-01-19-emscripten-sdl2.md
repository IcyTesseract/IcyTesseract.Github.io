---
layout: post
title: Emscripten with SDL2
---

Hello everyone, I just wanted to make a quick post about using SDL2 with Emscripten as this is not very commonly talked about anywhere on the internet (at least where I could find).

Also, I haven't posted in a long time (which I said I would post frequently so I'm trying to get back into blogging).

Anyways, there is a problem, SDL2 support is iffy with Emscripten, but support exists. What I mean by iffy is that SDL2 is not even included by default. It must be included and built on demand whenever building a program.

<!-- more -->

Speaking of programs, Here's a small program below:

{% highlight c linenos %}
#include "util.h"
#include "config.h"

#include <stdio.h>
#include <SDL.h>
#include <emscripten.h>

SDL_Window *window;

void log_error(const char *msg, const char *error_msg) {
  fprintf(stderr, "ERROR: %s -> %s!\n", msg, error_msg);
}

void render_loop() {
  glClear(GL_COLOR_BUFFER_BIT);
  SDL_GL_SwapWindow(window);
}

const char *v_src =
    "attribute vec3 v_pos;                \n"
    "void main() {                        \n"
    "    gl_Position = vec4(v_pos, 1.0);  \n"
    "}                                    \n";

const char *f_src =
    "void main() {                                \n"
    "    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n"
    "}                                            \n";

int main() {
  GLuint prog;

  // SDL2 INIT
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    log_error("SDL_Init() failed", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  window = SDL_CreateWindow(
      WIN_TITLE,
      0,
      0,
      WIN_WIDTH,
      WIN_HEIGHT,
      SDL_WINDOW_OPENGL);

  if (!window) {
    log_error("SDL_CreateWindow failed", "");
    exit(EXIT_FAILURE);
  }

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  SDL_GLContext context = SDL_GL_CreateContext(window);

  if (!context) {
    log_error("SDL_GL_CreateContext() failed", "");
    exit(EXIT_FAILURE);
  }

  // OPENGL INIT
  prog = load_program(v_src, f_src);

  if (!prog) {
    log_error("load_program() failed", "");
    exit(EXIT_FAILURE);
  }

  glClearColor(0.2, 0.2, 0.2, 1.0);

  emscripten_set_main_loop(render_loop, 0, 0);
}
{% endhighlight %}

This program pretty much just makes a window and sets up a program (this is not shown nor does it matter for this simple example where windowing is the focus). `Config.h` is just a file that declares:

- `WIN_TITLE`
- `WIN_WIDTH`
- `WIN_HEIGHT`

You should take note of a few things. The first is that there is no while loop. This is because a while loop will block the main thread to the point of freezing Chrome, Firefox, or whatever browser you're using. Thus, you have to use `emscripten_set_main_loop` with the method you would like to be called every frame. Another thing to notice is that I'm calling `SDL_Init(SDL_INIT_VIDEO)` instead of `SDL_Init(SDL_INIT_EVERYTHING` as currently that causes some issues with SDL2 not being built with proper threading support (currently working this out with Kripken (one of the developers actively working on the project).

This example would not be complete with a great `Makefile` for building. That's key here because there aren't many tutorials about using Emscripten especially with SDL2.

{% highlight Makefile linenos %}
CC = emcc

SRCS = main.c util.c
FILES = $(addprefix src/, $(SRCS)) # Add 'src/' to each source
OBJS = $(FILES:.c=.o) # Modify file extensions of FILES

EOPT = USE_SDL=2 FULL_ES2=1 # Emscripten specific options
EOPTS = $(addprefix -s $(EMPTY), $(EOPT)) # Add '-s ' to each option

# Builds necessary files
build: $(OBJS)
        mkdir build
        $(CC) $(FILES) -O2 $(EOPTS) -o build/index.html

# Builds necessary files and cleans up object files, but leaves build directory
dist: build
        rm $(OBJS)

# Cleans up object files and build directory
clean:
        rm -rf build
        rm $(OBJS)
{% endhighlight %}

In this case you'll see I'm building: `main.c` and `util.c`. In order to build, you just type in `make` in your terminal and the following output will be displayed:

{% highlight sh linenos %}
[harry@harry-laptop Emscripten-WebGL-Engine2]$ make
emcc    -c -o src/main.o src/main.c
emcc    -c -o src/util.o src/util.c
mkdir build
emcc src/main.c src/util.c  -O2 -s USE_SDL=2 -s FULL_ES2=1  -o build/index.html
WARNING  root: including port: sdl2
WARNING  root: building port: sdl2
In file included from /usr/lib/emscripten/tools/optimizer/parser.cpp:2:
In file included from /usr/lib/emscripten/tools/optimizer/parser.h:7:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/iostream:39:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/ostream:38:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/ios:38:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/iosfwd:40:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/bits/postypes.h:40:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/cwchar:44:
/usr/include/wchar.h:39:11: fatal error: 'stdarg.h' file not found
# include <stdarg.h>
          ^
1 error generated.
In file included from /usr/lib/emscripten/tools/optimizer/simple_ast.cpp:2:
In file included from /usr/lib/emscripten/tools/optimizer/simple_ast.h:3:
/usr/include/stdlib.h:32:10: fatal error: 'stddef.h' file not found
#include <stddef.h>
         ^
1 error generated.
In file included from /usr/lib/emscripten/tools/optimizer/optimizer.cpp:2:
In file included from /usr/lib64/gcc/x86_64-unknown-linux-gnu/4.9.2/../../../../include/c++/4.9.2/cstdio:42:
/usr/include/stdio.h:33:11: fatal error: 'stddef.h' file not found
# include <stddef.h>
          ^
1 error generated.
{% endhighlight %}

Yes, there are some errors. These are also being worked out with Kripken right now.

If you're planning on using this with your application, you'll need to make sure you have a list of your files there (`src/` is automatically put at the front of each file name). Hope this helps!
